package com.enjoyingfoss.feeel.presenter

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.os.Parcel
import android.os.Parcelable
import com.enjoyingfoss.feeel.WorkoutContract
import com.enjoyingfoss.feeel.WorkoutRepository
import com.enjoyingfoss.feeel.data.ExerciseMeta
import com.enjoyingfoss.feeel.data.Workout
import com.enjoyingfoss.feeel.view.WorkoutAudio
import java.lang.ref.WeakReference
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

/**
@author Miroslav Mazel
 */

//todo

//todo render state along with countdown immediately (esp. when going through exercises)
class WorkoutService : Service(), WorkoutContract.Presenter {

    enum class ExerciseStage { PREPARING, COUNTDOWN, EXERCISE, BREAK, DONE } //todo check that proguard handles enums effectively

    //todo rotation change sometimes skips an exercise
    companion object { //todo add notification
        private val COUNTDOWN_LENGTH = 3
    }

    private var views = ArrayList<WorkoutContract.View>(2)
    private val exerciseRetriever = WorkoutRepository()
    private val exerciseExecutor = Executors.newScheduledThreadPool(1)
    private var audioView: WorkoutAudio? = null

    private var state = InternalState(workout = exerciseRetriever.retrieveWorkout())
    private var future: Future<*>? = null

    //
    // InternalState class
    //

    private class InternalState(val workout: Workout,
                                var exercisePos: Int = 0,
                                var timeRemaining: Int = 0,
                                var stage: ExerciseStage = ExerciseStage.PREPARING,
                                var isTimerStopped: Boolean = true,
                                var isAudioOn: Boolean = true) : Parcelable {
        val curExerciseMeta: ExerciseMeta?
            get() = workout.exerciseMetas.getOrNull(exercisePos)

        val curExerciseLength: Int
            get() = workout.exerciseMetas.getOrNull(exercisePos)?.duration ?: 0

        val isFirstExercise: Boolean
            get() = exercisePos == 0

        val isLastExercise: Boolean
            get() = exercisePos == workout.size - 1

        val hasNoExercise: Boolean
            get() = workout.size == 0

        constructor(parcel: Parcel) : this(
                parcel.readParcelable(Workout::class.java.classLoader),
                parcel.readInt(),
                parcel.readInt(),
                ExerciseStage.values()[parcel.readInt()],
                parcel.readByte() != 0.toByte(),
                parcel.readByte() != 0.toByte())

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeParcelable(workout, flags)
            parcel.writeInt(exercisePos)
            parcel.writeInt(timeRemaining)
            parcel.writeInt(stage.ordinal)
            parcel.writeByte(if (isTimerStopped) 1 else 0)
            parcel.writeByte(if (isAudioOn) 1 else 0)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<InternalState> {
            override fun createFromParcel(parcel: Parcel): InternalState {
                return InternalState(parcel)
            }

            override fun newArray(size: Int): Array<InternalState?> {
                return arrayOfNulls(size)
            }
        }
    }

    //
    // Lifecycle
    //

    inner class XRCBinder : Binder() {
        val service: WeakReference<WorkoutService>
            get() = WeakReference(this@WorkoutService)
    }

    override fun onBind(intent: Intent): IBinder = XRCBinder()

    override fun onDestroy() {
        super.onDestroy()
        audioView?.shutdown()
        exerciseExecutor.shutdown() //todo see if necessary
        views.clear()
    }

    private fun startService() {
        enableAudio()
        setupNext()
    }

    private fun restoreService(restoredState: InternalState) {
        state = restoredState

        if (state.isAudioOn) enableAudio() //todo does this belong here?
        else disableAudio()

        if (!state.isTimerStopped) startTimer()

        rerender()
    }

    //
    // Stage switching
    //

    private fun setupNext() { //todo make a test for switching states correctly
        when (state.stage) {
            ExerciseStage.PREPARING -> {
                if (state.hasNoExercise) startDoneStage()
                else startCountdownStage()
            }

            ExerciseStage.COUNTDOWN -> startExerciseStage()

            ExerciseStage.EXERCISE -> {
                if (state.isLastExercise) startDoneStage()
                else startBreakStage()
            }

            ExerciseStage.BREAK -> startExerciseStage()
        }

        renderStage()
        if (isExerciseTimerStage()) renderPausePlay()
    }

    private fun startCountdownStage() {
        state.stage = ExerciseStage.COUNTDOWN
        state.timeRemaining = COUNTDOWN_LENGTH
        startTimer()
    }

    private fun startExerciseStage() {
        state.stage = ExerciseStage.EXERCISE
        state.timeRemaining = state.curExerciseLength
    }

    private fun startBreakStage() {
        state.exercisePos++
        state.stage = ExerciseStage.BREAK
        state.timeRemaining = state.workout.breakLength
    }

    private fun startDoneStage() {
        state.stage = ExerciseStage.DONE
        stopTimer()
    }

    //
    // View render
    //

    private fun rerender() {
        renderStage()
        if (isExerciseTimerStage()) renderPausePlay()
        for (view in views) view.setSeconds(state.timeRemaining)
    }

    private fun renderStage() {
        when (state.stage) {
            ExerciseStage.COUNTDOWN -> for (view in views) view.setupCountdown()
            ExerciseStage.EXERCISE -> for (view in views) view.setExercise(state.curExerciseMeta!!) //todo double-check !! assertion
            ExerciseStage.BREAK -> for (view in views) view.setBreak(state.curExerciseMeta!!)
            ExerciseStage.DONE -> for (view in views) view.setupDone()
        }
    }

    private fun renderPausePlay() {
        if (!state.isTimerStopped) for (view in views) view.setPlaying()
        else for (view in views) view.setPaused()
    }

    //
    // Timer
    //
    private val secondCounter = Runnable {
        //todo is a try catch needed here?
        countSecond()
    }

    private fun countSecond() {
        if (state.timeRemaining <= 0) {
            setupNext()
        }

        for (view in views) view.setSeconds(state.timeRemaining)

        state.timeRemaining -= 1
    }

    private fun startTimer() {
        if (future == null) { //todo double-check if using futures right
            state.isTimerStopped = false
            future = exerciseExecutor.scheduleAtFixedRate(secondCounter, 0, 1, TimeUnit.SECONDS)
        }
    }

    private fun stopTimer() {
        future?.cancel(true) //todo double-check if using futures right
        future = null
        state.isTimerStopped = true
    }

    private fun isExerciseTimerStage() = state.stage == ExerciseStage.EXERCISE || state.stage == ExerciseStage.BREAK

    //
    // WorkoutContract.Presenter
    //
    /* override fun changeWorkout(workout: Workout) {
        this.state = InternalState(workout = workout)
    } */

    override fun setView(callback: WorkoutContract.View, savedState: Parcelable?) {
        views.add(callback)

        if (state.stage != ExerciseStage.PREPARING) {
            rerender()
        }
        else if (savedState != null) {
            restoreService(savedState as InternalState)
        }
        else {
            startService()
        }
    }

    override fun disconnect(view: WorkoutContract.View) {
        //todo disconnect audio IF no view or notification?
        views.remove(view)
    }

    override fun getSavedState(): Parcelable = state

    override fun togglePlayPause() {
        if (isExerciseTimerStage()) {
            if (state.isTimerStopped) {
                startTimer()
            } else {
                stopTimer()
            }
            renderPausePlay()
        }
    }

    override fun skipToPreviousExercise() {
        if (!state.isFirstExercise) {
            state.timeRemaining = state.curExerciseLength
            state.exercisePos--

            if (state.stage != ExerciseStage.EXERCISE) startExerciseStage()

            renderStage()
        }
    }

    override fun skipToNextExercise() {
        if (!state.isLastExercise) {
            state.timeRemaining = state.curExerciseLength
            state.exercisePos++

            if (state.stage != ExerciseStage.EXERCISE) startExerciseStage()

            renderStage()
        }
    }

    private fun isAudioEnabled() = audioView != null

    private fun enableAudio() {
        if (!isAudioEnabled()) {
            val workoutAudio = WorkoutAudio(WeakReference(this))
            views.add(workoutAudio)
            audioView = workoutAudio
            state.isAudioOn = true
        }
    }

    private fun disableAudio() {
        if (isAudioEnabled()) {
            views.remove(audioView!!)
            audioView?.shutdown()
            audioView = null
            state.isAudioOn = false
        }
    }
}